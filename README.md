Description Menu

Hello dear user, this is an employee database program in C. 

When you run the program, it should print out a menu of options as the following:

1. Print the Database
2. Lookup employee by ID
3. Lookup employee by last name
4. Add an Employee
5. Quit

Please type number 1-5 to indicate your option. The program will read in your selection, perform the chosen operation on the employee database, and repeat until told to quit. i.e. menu option 2 will look up by ID.
It will continue handling your choices until the user enters 5, the "QUIT" option.



In this program, we assume the followings:
- no first nor last name is longer than 64 characters, including the trailing '\0' character.
- six digit ID value must be between 100000 and 999999 inclusive.
- salary amounts must be between $30,000 and $150,000 inclusive.
- the salary is a positive whole number amount (no decimals).
- every employee has exactly two names (a first and a last), so "John D. Rockefeller" is not a valid name 
- there are never more than 1024 employees.