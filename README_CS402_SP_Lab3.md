Description Menu

This program is for purpose of calculating a data set's average, median and standard deviation.  

File Format
The input file format consists of several lines of ASCII text. Each line contains a float number in the data set.   There can be any number of float numbers in the ASCII file.  

The program dynamically allocate memory size the data size you use.   

You need to build the .c file basicstats.c and then generate a excutable file named basicstats.    Then you can execute the file along with the name of the dataset you want to calculate.  

For example, you can use "basicstats large.txt" input to generate the results for the data set in the file "large.txt".

The output will be in the following format:

Results:
--------------------
Num Value: 12348
     Mean: 49.857
   Median: 49.048
  Std Dev: 28.781

Used Array Capacity:8132




