Description Menu

Dear user, this is an employee database program in C. 

This program takes one command line argument, which is the name of the input file containing employee information, to load the employee data stored in the file "input.txt" .

File Format
The input file format consists of several lines of ASCII text. Each line contains the information for one employee in the following order:

six_digit_ID  first_name  last_name  salary
For example, here is a file with 1 employees:

273836 Edsger Dijkstra 93000

A small library that includes functions will be used. These library files are readfile.c and readfile.h

Here are the functiones included in the library:

Open a file by calling: open_file(), passing in the name of the file to open as a string; open_file() returns 0 if the file is successfully opened, and -1 if the file cannot be opened.
Functions to read values of different types into program variables: read_int(), read_string(), read_float(). These functions take arguments much like scanf does: they need to know the memory location of where to put the value read in. 

When you run the program, it should print out a menu of options as the following:

1. Print the Database
2. Lookup employee by ID
3. Lookup employee by last name
4. Add an Employee
5. Quit
6. Remove an employee
7. Update an employee's information
8. Print the M employees with the highest salaries
9. Find all employees with matching last name

Please type number 1-9 to indicate your option. The program will read in your selection, perform the chosen operation on the employee database, and repeat until told to quit. i.e. menu option 8 will print highest salaries. When you type 8, the program would ask you to enter a value for M (e.g. 5), and then print out the employee information for the M (5) employees with the highest salary. If this number is higher than the maximum number of the database, it will print out the entire databaze. 

It will continue handling your choices until the user enters 5, the "QUIT" option.

In this program, we assume the followings:
- The matching should be case insensitive
- no first nor last name is longer than 64 characters, including the trailing '\0' character.
- six digit ID value must be between 100000 and 999999 inclusive.
- salary amounts must be between $30,000 and $150,000 inclusive.
- the salary is a positive whole number amount (no decimals).
- every employee has exactly two names (a first and a last), so "John D. Rockefeller" is not a valid name 
- there are never more than 1024 employees.