#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"readfile.h"

//print out a menu of transaction option
int menu()
{
        int option;
        int ret;
        printf("\nEmplyee DB Menu:\n");
        printf("**********************************\n");
        printf("1.Print the Database\n");
        printf("2.Lookup employee by ID\n");
        printf("3.Lookup employee by last name\n");
        printf("4.Add an Employee\n");
        printf("5.Quit\n");
        printf("6.Remove an Employee\n");
        printf("7.Update an Employee's Information\n");
        printf("8.Print the M Employees with the Highest Salaries\n");
        printf("9.Find all Employees with Matching Last Name\n");
        printf("**********************************\n");
        printf("Enter your option:");
        ret = read_int(&option);
        return option;
}

// option 1: print the database
void printDB(struct person employee[MAXEMP], int num)
{
    printf("\nNAME                       SALARY    ID\n");
    printf("*********************************************\n");
    for(int i=0; i<num; i++){
        printf("%-10s %-10s %10d %10d \n", employee[i].firstname,employee[i].lastname,employee[i].salary,employee[i].id);
    }
    printf("*********************************************\n");
    printf("Number of Employees (%d)\n\n",num);
}

//sorted by id
int compare_person (struct person *a, struct person *b)
{
        return a->id - b->id;
}

//sorted by salary
int compare_person_salary (struct person *a, struct person *b)
{
        return a->salary - b->salary;
}

//binary search to find a matching employee in the database
int binary_search(const int arr[], int l, int r, int x)
{
    if(r >= l)
    {
        int mid = l+ (r-l)/2;
        if (arr[mid] == x) return mid;
        if (arr[mid] > x) return binary_search(arr, l, mid-1, x);
        return binary_search(arr, mid+1, r, x);
    }
    return -1;
}

//print the found employee
void print_by_index(struct person employee[MAXEMP], int index){
    printf("\nNAME                       SALARY    ID\n");
    printf("*********************************************\n");
    printf("%-10s %-10s %10d %10d \n", employee[index].firstname,employee[index].lastname,employee[index].salary,employee[index].id);
    printf("*********************************************\n\n");
}

//option 2: Lookup by id
void lookup_by_ID(struct person employee[MAXEMP], int num, int list[MAXEMP])
{
    int id;
    printf("\nEnter a 6-digit ID of Employee:\n");
    scanf("%d", &id);
    int index = binary_search(list, 0, num, id);
    if(index ==-1){
        printf("This ID is not found.\n");
    }
    else
    {
        print_by_index(employee, index);
    }
    return;
}

//compare target last name with last names in database
int search_lastname(struct person employee[MAXEMP], int num, char lastname[]){
    
    int temp=-1;
    for (int i=0; i<num; i++)
    {
        if(strcmp(employee[i].lastname, lastname)==0) temp=i;
    }
    return temp;
}

//option 3: Lookup by last name
void lookup_by_lastname(struct person employee[MAXEMP], int num)
{
    char lastname[MAXNAME];
    printf("\nEnter Last Name of Employee:\n");
    scanf("%s", lastname);
    
    int index = search_lastname(employee, num, lastname);
    
    if(index == -1) printf("Last name is not found\n");
    else print_by_index(employee, index);
    
    return;
}

//option 4: Add an employee
void add_employee(struct person (*employee)[MAXEMP], int *num, int (*list)[MAXEMP])
{
    char tempfirstname[MAXNAME], templastname[MAXNAME];
    int salary, confirm;
    int ret;
    printf("\nEnter First Name of Employee:\n");
    ret = read_string(tempfirstname);
    printf("Enter Last Name of Employee:\n");
    ret = read_string(templastname);
    printf("Enter Salary of Employee ($30,000,$150,000):\n");
    ret = read_int(&salary);
    printf("Do you confirm to add this employee? 1 for Yes and 0 for No.\n");
    ret = read_int(&confirm);

    if (confirm ==1)
    {
        if((salary >= 30000) && (salary<=150000))
        {
            int cur_id = (*employee)[(*num)-1].id;
            int new_id = cur_id+1;
            strcpy((*employee)[*num].firstname, tempfirstname);
            strcpy((*employee)[*num].lastname, templastname);
            (*employee)[*num].salary = salary;
            (*employee)[*num].id = new_id;
            (*list)[(*num)] = (*employee)[(*num)].id;
            (*num)++;
            return;
        }
        else printf("Salary input is invalid.\n");
        return;
    }
    else printf("Action of add cancelled.\n"); return;
}

//option 6:Remove an Employee
void remove_employee(struct person (*employee)[MAXEMP], int *num, int (*list)[MAXEMP])
{
    int id;
    printf("\nEnter a 6-digit ID of Employee:\n");
    scanf("%d", &id);
            
    int index = binary_search(*list, 0, *num, id);
    if (index == -1) printf("This ID is not found.\n");
    else print_by_index(*employee, index);
    
    int ret, confirm;
    printf("Do you confirm to remove this employee? 1 for Yes and 0 for No.\n");
    ret = read_int(&confirm);

    if (confirm == 1)
    {
        for (int i=index; i<(*num); i++){
            strcpy((*employee)[i].firstname, (*employee)[i+1].firstname);
            strcpy((*employee)[i].lastname, (*employee)[i+1].lastname);
            (*employee)[i].salary = (*employee)[i+1].salary;
            (*employee)[i].id = (*employee)[i+1].id;
            (*list)[i] = (*list)[i+1];
        }
        (*num)=(*num)-1;
        return;
    }
    else printf("Action of add cancelled.\n"); return;
}

//option 7: Update an Employee
void update_employee(struct person (*employee)[MAXEMP], int *num, int (*list)[MAXEMP])
{
    
    int id;
    printf("\nEnter a 6-digit ID of Employee:\n");
    scanf("%d", &id);
            
    int index = binary_search(*list, 0, *num, id);
    if (index == -1) printf("This ID is not found.\n");
    else print_by_index(*employee, index);
    
    char tempfirstname[MAXNAME], templastname[MAXNAME];
    int salary, ret, choice;
    
    printf("What information do you want to change? 1 for First Name, 2 for Last Name and 3 for Salary.\n");
    
    ret = read_int(&choice);
    
    if(choice ==1)
    {
        printf("\nEnter First Name of Employee:\n");
        ret = read_string(tempfirstname);
        strcpy((*employee)[index].firstname, tempfirstname);
        return;
    }
    else if(choice ==2)
    {
        printf("Enter Last Name of Employee:\n");
        ret = read_string(templastname);
        strcpy((*employee)[index].lastname, templastname);
        return;
    }
    else if(choice ==3)
    {
        printf("Enter Salary of Employee ($30,000,$150,000):\n");
        ret = read_int(&salary);
        if ((salary >= 30000) && (salary <= 150000)) (*employee)[index].salary = salary;
        else printf("Salary input is invalid.\n");
        return;
    }
    else
    {
        printf("Choice is not available, please try again\n");
        return;
    }
}

//option 8: Find the M highest salary employees
void hsalary_employee(struct person employee_salary[MAXEMP], int num)
{
    qsort(employee_salary, num, sizeof(struct person), compare_person_salary);
    int M = 0;
    printf("\nInput the number of employees with top salary please. \n");
    int ret;
    ret = read_int(&M);
    
    printf("\nNAME                       SALARY    ID\n");
    printf("*********************************************\n");
    int j=max(num-M,0);
    for (int i=j; i<num; i++) printf("%-10s %-10s %10d %10d \n", employee_salary[i].firstname,employee_salary[i].lastname,employee_salary[i].salary,employee_salary[i].id);
    printf("*********************************************\n\n");
    return;
}

//option 9: Find the employees with a certain last name
void lastname_employee(struct person employee[MAXEMP], int num)
{
    printf("\nInput the last name you want to search for. \n");
    char templastname[MAXNAME];
    int ret = read_string(&templastname);
    int check = 0;
    
    for (int i=0; i<num; i++)
    {
        if (strcmp(employee[i].lastname, templastname)==0)
        {
            if (check==0)
            {
                printf("\nNAME                       SALARY    ID\n");
                printf("*********************************************\n");
            }
            printf("%-10s %-10s %10d %10d \n", employee[i].firstname,employee[i].lastname,employee[i].salary,employee[i].id);
            check++;
        }
    }
    if (check>0) printf("*********************************************\n");
    if (check == 0) printf("Last name not found\n");
    return;
}

int main(int argc, char *argv[])
{

    char *fname = "input.txt", firstname[MAXNAME], lastname[MAXNAME];
    struct person employee[MAXEMP];
    struct person employee_temp[MAXEMP];
    int id, salary, option, index, confirm, num = 0, list[MAXEMP];

    FILE *fpointer = fopen(fname, "r");
    while(!feof(fpointer)){
        fscanf(fpointer,"%d %s %s %d\n", &employee[num].id, employee[num].firstname, employee[num].lastname, &employee[num].salary);
        list[num] = employee[num].id;
        num++;
    }
    fclose(fpointer);
    
    qsort(employee, num, sizeof(struct person), compare_person);
    
    int i=0;
    for (i=0;i<num;i++)
    {
        list[i]=employee[i].id;
    }    

    while (option!=5)
    {
        option = menu();
        if (option ==1) printDB(employee, num);
        else if (option ==2) lookup_by_ID(employee, num, list);
        else if (option ==3) lookup_by_lastname(employee, num);
        else if (option ==4) add_employee(&employee, &num, &list);
        else if (option ==6) remove_employee(&employee, &num, &list);
        else if (option ==7) update_employee(&employee, &num, &list);
        else if (option ==8)
        {
            memcpy(employee_temp, employee, sizeof(struct person)*MAXEMP);
            hsalary_employee(employee_temp, num);
        }
        else if (option ==9) lastname_employee(employee, num);
        else if (option !=5) printf("%d is not available, please try again\n", option); continue;
    }
    printf("\nThanks. Goodbye.\n\n");
}




