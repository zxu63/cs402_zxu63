//
//  basicstats.c
//  CS 402 SP Lab3
//
//  Created by Zhen Xu on 2020/4/24.
//

#include<stdio.h>
#include<stdlib.h>
#include<math.h>


// this function is used by qsort to sort the numbers set
int compare_number (float *a, float *b)
{
        return (*a)-(*b);
}

//calculate the mean of the numbers set, using input of the numbers set and its size
double get_mean(float *numbs, int index)
{
    double sum=0, mean=0;
    
    for (int i=0;i<index;i++){
        sum=sum+numbs[i];
    }
    
    mean = sum/index;
    return mean;
}

//calculate the median of the numbers set, using input of the numbers set and its size
double get_median(float *numbs, int index)
{
    double median=0;
    
    if (index%2==0) median = (numbs[index/2-1]+numbs[(index/2)])/2;
    else median = numbs[(index-1)/2];
    
    return median;
}

//calculate the standard deviation of the numbers set, using input of the numbers set, its size, and its mean
double get_stddev(float *numbs, int index, double mean)
{
    double sum=0, stddev=0;
    for (int i=0;i<index;i++){
        sum=sum+(numbs[i]-mean)*(numbs[i]-mean);
    }
    stddev=sqrt(sum/index);
    
    return stddev;
}

// main function
int main(int argc, char *argv[])
{
    char *fname = argv[1];
    float *numbers, *temp_numbers;
    int index = 0;
    int size = 20;
    
    numbers = (float *)malloc(size * sizeof(float));
    
    FILE *fpointer = fopen(fname, "r");
    while(!feof(fpointer))
    {
        fscanf(fpointer,"%f\n", &(numbers[index]));
        index++;
        if(index==size-1)
        {
            size=size*2;
            temp_numbers = (float *)malloc(size*sizeof(float));
            memcpy(temp_numbers,numbers,(index+1)*sizeof(float));
            free(numbers);
            numbers = temp_numbers;
        }
    }
    fclose(fpointer);
    qsort(numbers, index, sizeof(float), compare_number);
    
    double mean, median, stddev;
    mean = get_mean(numbers, index);
    median = get_median(numbers, index);
    stddev = get_stddev(numbers, index, mean);
    
    printf("\nResults:\n");
    printf("--------------------\n");
    printf("Num Value: %d\n", index);
    printf("     Mean: %.3lf\n", mean);
    printf("   Median: %.3lf\n", median);
    printf("  Std Dev: %.3lf\n\n", stddev);
    printf("Unused Array Capacity:%d\n\n", size-index);
    
    free(numbers);

    return 0;
}
